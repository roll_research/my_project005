import express from "express"
import Static from "serve-static"
import path from "path"
import bodyParser from "body-parser";
import expressErrorHandler from "express-error-handler";
import cookieParser from "cookie-parser";
import expressSession from "express-session";
import router from "./routers/index.js";
import mariadb from "./db/config.js";
import nodeSchedule from "node-schedule"
import axios from "axios";



const __dirname = path.resolve();

const errorHandler = expressErrorHandler({
    Static: {
        '404': './public/404.html'
    }
});


class App {

    constructor() {
        this.app = express()
        this.setMiddleWare()
        this.setSession()
        this.setStatic()
        this.getRouting()
        this.setErrorHandler()
        this.getMariaDB()
        this.schedulest();
    };

    setMiddleWare() {
        const port = process.env.PORT || 3000;
        this.app.set('port', process.env.PORT || 3000);
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());
        this.app.use('/node_modules', express.static(path.join(__dirname, '/node_modules')));
    };

    setSession() {
        this.app.use(cookieParser());
        this.app.use(expressSession({
            secret: 'my key',
            resave: true,
            saveUninitialized: true
        }));
    };

    getRouting() {
        this.app.use(router)
    }

    setStatic() {
        this.app.use('/', express.static(__dirname + '/public'));
    };

    setErrorHandler() {
        this.app.use(expressErrorHandler.httpError(404));
        this.app.use(errorHandler);
    };

    errorHandler() {
        this.app.use((req, res, _) => {
            res.status(404), render("404.html")
        });

        this.app.use((err, req, res, _) => {
            res.status(500).render("500.html");
        })
    };

    getMariaDB() {
        const conns = mariadb.connect((resolve) => {
            console.log('db 연결이 되었습니다.');
        });
    };

    schedulest() {
        const job = nodeSchedule.scheduleJob('* 4 * * * *', () => {
            //console.log('안녕');
            axios.post("http://localhost:3000/process/check_weather.do")
                .then(response => {
                    console.log('')
                    console.log(response.data);
                })
                .catch(error => {
                    console.error(error);
                })

            job.cancel();
        });
    }

};

export default new App().app;