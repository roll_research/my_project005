import DB from '../db/config.js'
import request from 'request';
import convert from 'xml-js';


class Repository {

    constructor(DB) {
        this.DB = DB;
        console.log('DB 정보가 repository에 연결되었습니다.');
    }

    async getMessage(req, res, year, month, day, title, message) {

        let gymd = year + '/' + month + '/' + day;
        let gtitle = title;
        let gmessage = message;

        await DB.query('INSERT INTO db_calender VALUES(?,?,?,?)', [gtitle, '오한성', gmessage, gymd], (err, rows, fields) => {
            if (!err) {
                console.log(year + '/' + month + '/' + day + '  [제목]: ' + title + ' [할 일]:' + message);

                res.send(year + '/' + month + '/' + day + '  [제목]: ' + title + ' [할 일]:' + message);
            } else {
                res.send('err:' + err);
            }
        })

    }

    async setMessage(req, res, year, month, day, message) {

        let gymd = year + '/' + month + '/' + day;
        let gtitle = '오늘의 할 일 [' + gymd + ']'
        let gmessage = message;

        await DB.query('INSERT INTO db_calender VALUES(?,?,?,?)', [gtitle, '오한성', gmessage, gymd], (err, rows, fields) => {
            if (!err) {
                console.log(year + '/' + month + '/' + day + '  [제목]: ' + gtitle + ' [할 일]: ' + message);

                res.send(year + '/' + month + '/' + day + '  [제목]: ' + gtitle + ' [할 일]: ' + message);
            } else {
                res.send('err:' + err);
            }
        });
    }

    async getinfo(req, res) {
        await this.DB.query('SELECT * FROM db_calender', (err, rows, fields) => {
            if (!err) {
                let result = rows;
                console.log(result);
                res.send({ result: result });
            } else {
                console.log("err: " + err);
                res.send(err);
            }
        })
    }


    async getweather(req, res) {
        var service_key = 'dIKjWIBLjlM%2BuZXUspeyUu2s479dUgsIVws1DDUg5OY82UF3HvvfYTZBYPDvELyGyK0FU8lBWWXMZxtx%2BKiM9A%3D%3D';


        var url = 'http://apis.data.go.kr/1360000/AsosHourlyInfoService/getWthrDataList';
        var queryParams = '?' + encodeURIComponent('serviceKey') + '=' + service_key; /* Service Key*/
        queryParams += '&' + encodeURIComponent('pageNo') + '=' + encodeURIComponent('1'); /* */
        queryParams += '&' + encodeURIComponent('numOfRows') + '=' + encodeURIComponent('10'); /* */
        queryParams += '&' + encodeURIComponent('dataType') + '=' + encodeURIComponent('XML'); /* */
        queryParams += '&' + encodeURIComponent('dataCd') + '=' + encodeURIComponent('ASOS'); /* */
        queryParams += '&' + encodeURIComponent('dateCd') + '=' + encodeURIComponent('HR'); /* */
        queryParams += '&' + encodeURIComponent('startDt') + '=' + encodeURIComponent('20221227'); /* */
        queryParams += '&' + encodeURIComponent('startHh') + '=' + encodeURIComponent('21'); /* */
        queryParams += '&' + encodeURIComponent('endDt') + '=' + encodeURIComponent('20221227'); /* */
        queryParams += '&' + encodeURIComponent('endHh') + '=' + encodeURIComponent('21'); /* */
        queryParams += '&' + encodeURIComponent('stnIds') + '=' + encodeURIComponent('108'); /* */

        await request({
            url: url + queryParams,
            method: 'GET',
        }, function(error, response, body) {
            //console.log('Status', response.statusCode);
            //console.log('Headers', JSON.stringify(response.headers));
            //console.log('Reponse received', body);

            let parser = convert.xml2json(body, { compact: true, spaces: 4 });
            console.log(parser);

            let obj = JSON.parse(parser);


            let result = '지역:' + obj.response.body.items.item.stnNm._text + ' 기온:' + obj.response.body.items.item.ta._text;

            //res.send(obj.response.body.items.item.stnNm._text);
            res.send(result);

            //res.send(parser.items);

        })

    }



}

export default new Repository(DB);