import express from "express";
import DB from "../db/config.js";
import repository from "../repository/repository.js";

let router = express.Router();

router.get("/check", (req, res) => {
    console.log('/process/check 호출');

    res.send('This is good');
});

router.post("/checks", (req, res) => {
    console.log('/process/checks 호출');

    res.send('This is goods!!!!!');
});

router.post("/check_weather.do", (req, res) => {
    console.log('/process/check_weather.do 호출');

    repository.getweather(req, res);


})




router.get('/getMessage.do', (req, res) => {
    let year = req.query.year;
    let month = req.query.month;
    let day = req.query.day;
    let title = req.query.title;
    let message = req.query.message;

    if (year.length === 2) {
        year = '20' + year;
    }

    repository.getMessage(req, res, year, month, day, title, message);


    /*
    if (year.length === 2) {
        year = '20' + year;
    }

    let gymd = year + '/' + month + '/' + day;
    let gtitle = title;
    let gmessage = message;

    DB.query('INSERT INTO db_calender VALUES(?,?,?,?)', [gtitle, '오한성', gmessage, gymd], (err, rows, fields) => {
        if (!err) {
            console.log(year + '/' + month + '/' + day + '  [제목]: ' + title + ' [할 일]:' + message);

            res.send(year + '/' + month + '/' + day + '  [제목]: ' + title + ' [할 일]:' + message);
        } else {
            res.send('err:' + err);
        }
    })
    */
});

router.post('/setMessage.do', (req, res) => {
    let year = req.query.year_on || req.body.year_on;
    let month = req.query.month_on || req.body.month_on;
    let day = req.query.day_on || req.body.day_on;
    let message = req.query.message || req.body.message;


    repository.setMessage(req, res, year, month, day, message);

    /*
    let gymd = year + '/' + month + '/' + day;
    let gtitle = '오늘의 할 일 [' + gymd + ']'
    let gmessage = message;


    DB.query('INSERT INTO db_calender VALUES(?,?,?,?)', [gtitle, '오한성', gmessage, gymd], (err, rows, fields) => {
        if (!err) {
            console.log(year + '/' + month + '/' + day + '  [제목]: ' + gtitle + ' [할 일]: ' + message);

            res.send(year + '/' + month + '/' + day + '  [제목]: ' + gtitle + ' [할 일]: ' + message);
        } else {
            res.send('err:' + err);
        }
    })
    */
});

router.post('/getinfo.do', (req, res) => {
    repository.getinfo(req, res);
});




export default router;